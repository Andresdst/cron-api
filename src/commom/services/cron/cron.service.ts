import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { HttpService } from '@nestjs/axios';
import { firstValueFrom } from 'rxjs';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Note } from 'src/notes/schemas/note.schema';

@Injectable()
export class CronService {
  constructor(
    @InjectModel(Note.name) private noteModel: Model<Note>,
    private http: HttpService,
  ) {
    this.filldata();
  }

  @Cron('*/60 * * * *')
  async handleCron() {
    const response = this.http.get(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );
    const notes = await firstValueFrom(response);

    notes.data.hits.forEach((element, index) => {
      if (index === 0) {
        console.log('se inserto', index);
        const newNote = new this.noteModel(element);
        return newNote.save();
      }
    });
  }

  async filldata() {
    const response = this.http.get(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );
    const notes = await firstValueFrom(response);

    notes.data.hits.forEach((element) => {
      const newNote = new this.noteModel(element);
      return newNote.save();
    });
  }
}
