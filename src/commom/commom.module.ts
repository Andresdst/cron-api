import { Module } from '@nestjs/common';
import { CronService } from './services/cron/cron.service';
import { HttpModule } from '@nestjs/axios';
import { MongooseModule } from '@nestjs/mongoose';
import { Note, NoteSchema } from 'src/notes/schemas/note.schema';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([
      {
        name: Note.name,
        schema: NoteSchema,
      },
    ]),
  ],
  providers: [CronService],
})
export class CommomModule {}
