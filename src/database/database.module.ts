import { Module, Global } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MongoClient } from 'mongodb';

@Global()
@Module({
  imports: [
    MongooseModule.forRoot('mongodb://mongo:27017', {
      user: 'root',
      pass: 'root',
      dbName: 'notes-db',
    }),
  ],
  providers: [
    {
      provide: 'MONGO',
      useFactory: async () => {
        const uri = `${process.env.MONGO_CONNECTION}://${process.env.MONGO_INITDB_ROOT_USERNAME}:${process.env.MONGO_INITDB_ROOT_PASSWORD}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/?authSource=admin&readPreference=primary`;

        const cliente = new MongoClient(uri);
        await cliente.connect();

        const database = cliente.db(process.env.MONGO_DB);
        return database;
      },
    },
  ],
  exports: ['MONGO', MongooseModule],
})
export class DatabaseModule {}
