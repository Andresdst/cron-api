import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BodyUserSchema, User } from '../schemas/user.schema';
import * as bcrypt from 'bcrypt';

import { Model } from 'mongoose';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<User>) {}

  async findByEmail(email: string) {
    const user = await this.userModel.findOne({ email }).exec();
    if (!user) {
      throw new NotFoundException(`User #${email} not found`);
    }
    return user;
  }

  async create(payload: BodyUserSchema) {
    const newUser = new this.userModel(payload);
    const hashPass = await bcrypt.hash(newUser.password, 10);
    newUser.password = hashPass;
    
    return await newUser.save();
  }
}
