import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { BodyUserSchema, User } from '../schemas/user.schema';
import { UsersService } from '../services/users.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('users')
@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get(':id')
  get(@Param('id') id: string) {
    return this.usersService.findByEmail(id);
  }

  @Post()
  create(@Body() body: BodyUserSchema) {
    return this.usersService.create(body);
  }
}
