import {
  Get,
  Delete,
  HttpStatus,
  Param,
  HttpCode,
  NotFoundException,
  Controller,
  Post,
  Body,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { NotesService } from 'src/notes/services/notes.service';
import { ApiTags } from '@nestjs/swagger';

import {
  Note,
  FilterNotesSchema,
  BodyNotesSchema,
} from '../schemas/note.schema';
import { ApikeyGuard } from 'src/auth/guards/apikey.guard';

@ApiTags('notes')
@UseGuards(AuthGuard('jwt'))
@Controller('notes')
export class NotesController {
  constructor(private notesService: NotesService) {}

  @Get()
  getByFilters(
    @Query() params: FilterNotesSchema,
    @Body() body: BodyNotesSchema,
  ) {
    let notes: Promise<Note[]>;
    if (body) {
      notes = this.notesService.findByFilters(body);
    } else {
      notes = this.notesService.findAll(params);
    }
    if (!notes) {
      throw new NotFoundException('there is no product');
    }
    return notes;
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK) // 👈 Using decorator
  GetOne(@Param('id') id: string) {
    return this.notesService.findOne(id);
  }

  @Post()
  Create(@Body() payload: Note) {
    return this.notesService.create(payload);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.OK) // 👈 Using decorator
  DeleteOne(@Param('id') productId: string) {
    return this.notesService.deleteOne(productId);
  }
}
