import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, FilterQuery } from 'mongoose';
import {
  FilterNotesSchema,
  Note,
  BodyNotesSchema,
} from '../schemas/note.schema';

@Injectable()
export class NotesService {
  constructor(@InjectModel(Note.name) private noteModel: Model<Note>) {}

  async findAll(params?: FilterNotesSchema) {
    if (params.offset) {
      console.log('params', params);
      const { offset } = params;

      return await this.noteModel
        .find()
        .skip((offset - 1) * 5)
        .limit(5)
        .exec();
    }
    return await this.noteModel.find().exec();
  }

  async findByFilters(body?: BodyNotesSchema) {
    const filters: FilterQuery<Note> = {};
    const { author, title, tags } = body;
    if (author) {
      filters.author = author;
    }
    if (title) {
      filters.title = title;
    }
    if (tags) {
      filters._tags = { $in: tags };
    }
    return await this.noteModel.find(filters).exec();
  }

  async findOne(id: string) {
    const note = await this.noteModel.findById(id).exec();
    if (!note) {
      throw new NotFoundException(`Product #${id} not found`);
    }
    return note;
  }

  create(data: Note) {
    const newNote = new this.noteModel(data);
    return newNote.save();
  }

  deleteOne(id: string) {
    return this.noteModel.findByIdAndDelete(id);
  }
}
