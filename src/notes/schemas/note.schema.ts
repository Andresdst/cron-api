import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import {
  IsNumber,
  Min,
  IsOptional,
  IsPositive,
  IsString,
} from 'class-validator';
import { Highligh } from './highligh.schema';

@Schema()
export class Note extends Document {
  @Prop()
  author: string;

  @Prop()
  created_at: Date;

  @Prop()
  title?: string;

  @Prop()
  url?: string;

  @Prop()
  points?: number;

  @Prop()
  story_text?: string;

  @Prop()
  comment_text?: string;

  @Prop()
  num_comments?: number;

  @Prop()
  story_id?: number;

  @Prop()
  story_title?: string;

  @Prop()
  story_url?: string;

  @Prop()
  parent_id?: number;

  @Prop()
  created_at_i?: number;

  @Prop()
  _tags: Array<string>;

  @Prop()
  objectID?: string;

  @Prop()
  _highlightResult?: Highligh;
}

export const NoteSchema = SchemaFactory.createForClass(Note);

export class FilterNotesSchema {
  @IsOptional()
  @IsPositive()
  @Min(1)
  offset: number;
}

export class BodyNotesSchema {
  @IsOptional()
  @IsString()
  author: string;

  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  tags: Array<string>
}
