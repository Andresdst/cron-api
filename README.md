<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Description

Aplicacion dockerizada de notes con logica de usuarios para poder eliminar notas, la db se autogenera y guarda +1 nota por hora

## Installation

```bash
$ npm install
$ docker-compose build
```

## Running the app

```bash

$ docker-compose up

```
--------- WARNING! --------

esperar a que NestJs levante, en caso de no levantar, ejecutar de nuevo


## Descargar cliente

Para interactuar con la App descargar Postman
- Website - [https://www.postman.com/](https://www.postman.com/)

Importar coleccion de endpoints a postman: 
- files-> import -> \cron-api\cron-api-nest.postman_collection.json

## Create a new user

Enviar en el request body email y password a la siguiente ruta de la coleccion user
- ruta - [http://localhost:5000/users/](http://localhost:5000/users/)

Request Body:

```bash
{
    "email": "prueba@prueba.com",
    "password": "123456"
}
```

## logear al nuevo usuario creado

Enviar en el request body email y password a la siguiente ruta de la coleccion user
- ruta - [http://localhost:5000/auth/login](http://localhost:5000/auth/login)

Request Body:

```bash
{
    "email": "prueba@prueba.com",
    "password": "123456"
}
```
.
Copiar token generado en el porta papeles (la sesion dura 1 dia con el servidor en ejecucion)

## Interactuar con la coleccion notes

para poder consultar o eliminar una de las notas debe enviar el token de la sesion por medio del header de autorizacion de tipo bearer token
- ruta - [http://localhost:5000/notes?offset=1](http://localhost:5000/notes?offset=1)
offset representa la pagina que quiere consultar
(las paginas traeran un max de 5 notas)

tambien puede consultar la documentacion de la api en swagger

- ruta - [http://localhost:5000/docs](http://localhost:5000/docs)